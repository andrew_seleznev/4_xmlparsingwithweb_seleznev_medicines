<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<link type="text/css" rel="stylesheet"
	href="<c:url value="/css/style.css" />" />
<head>
<title>Welcome</title>

</head>
<body>
	<h3>Parse XML page</h3>

	<form name="parsePage" method="post" action="controller">
		<input type="hidden" name="command" value="parse" /> XML parsing type
		<select name="parser">
			<option value="">Choose a type...</option>
			<option value="sax">SAX</option>
			<option value="dom">DOM</option>
			<option value="stax">StAX</option>
		</select> <input type="reset" value="Clear" name="clear" /> <input
			type="submit" value="Parse" name="submit" /> <br>
		Parser type:
		<c:out value="${currentParserType}" />
		<br> Medicines type:
		<c:out value="Analgesic" />
		<div class="result_table">
			<table>
				<tr>
					<th>Title</th>
					<th>ID</th>
					<th>Pharm</th>
					<th>Analogs</th>			
					<th>Dosage</th>
					<th>Periodicity</th>
					<th>Package Type</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Version</th>
					<th>Sertificate №</th>
					<th>Date</th>
					<th>Organisation</th>
					<th>Type</th>
				</tr>
				<c:forEach var="elem" items="${analgesic}" varStatus="status">
					<tr>
						<td><c:out value="${elem.title}" /></td>
						<td><c:out value="${elem.medicamentId}" /></td>
						<td><c:out value="${elem.pharm}" /></td>
						<td><c:out value="${elem.analogs}" /></td>
						<td><c:out value="${elem.dosageRegimen.dosage}" /></td>
						<td><c:out value="${elem.dosageRegimen.periodicity}" /></td>
						<td><c:out value="${elem.pack.packageType}" /></td>
						<td><c:out value="${elem.pack.quantity}" /></td>
						<td><c:out value="${elem.pack.price}" /></td>
						<td><c:out value="${elem.version}" /></td>
						<td><c:out value="${elem.sertificate.sertificateNumber}" /></td>
						<td><c:out value="${elem.sertificate.date}" /></td>
						<td><c:out value="${elem.sertificate.organisation}" /></td>
						<td><c:out value="${elem.classificationType}" /></td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<br> Medicines type:
		<c:out value="Antibiotic" />
		<div class="result_table">
			<table>
				<tr>
					<th>Title</th>
					<th>ID</th>
					<th>Pharm</th>
					<th>Analogs</th>			
					<th>Dosage</th>
					<th>Periodicity</th>
					<th>Package Type</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Version</th>
					<th>Sertificate №</th>
					<th>Date</th>
					<th>Organisation</th>
					<th>Type</th>
				</tr>
				<c:forEach var="elem" items="${antibiotic}" varStatus="status">
					<tr>
						<td><c:out value="${elem.title}" /></td>
						<td><c:out value="${elem.medicamentId}" /></td>
						<td><c:out value="${elem.pharm}" /></td>
						<td><c:out value="${elem.analogs}" /></td>
						<td><c:out value="${elem.dosageRegimen.dosage}" /></td>
						<td><c:out value="${elem.dosageRegimen.periodicity}" /></td>
						<td><c:out value="${elem.pack.packageType}" /></td>
						<td><c:out value="${elem.pack.quantity}" /></td>
						<td><c:out value="${elem.pack.price}" /></td>
						<td><c:out value="${elem.version}" /></td>
						<td><c:out value="${elem.sertificate.sertificateNumber}" /></td>
						<td><c:out value="${elem.sertificate.date}" /></td>
						<td><c:out value="${elem.sertificate.organisation}" /></td>
						<td><c:out value="${elem.chemicalStructureType}" /></td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<br> Medicines type:
		<c:out value="Vitamin" />
		<div class="result_table">
			<table>
				<tr>
					<th>Title</th>
					<th>ID</th>
					<th>Pharm</th>
					<th>Analogs</th>			
					<th>Dosage</th>
					<th>Periodicity</th>
					<th>Package Type</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Version</th>
					<th>Sertificate №</th>
					<th>Date</th>
					<th>Organisation</th>
					<th>Type</th>
				</tr>
				<c:forEach var="elem" items="${vitamin}" varStatus="status">
					<tr>
						<td><c:out value="${elem.title}" /></td>
						<td><c:out value="${elem.medicamentId}" /></td>
						<td><c:out value="${elem.pharm}" /></td>
						<td><c:out value="${elem.analogs}" /></td>
						<td><c:out value="${elem.dosageRegimen.dosage}" /></td>
						<td><c:out value="${elem.dosageRegimen.periodicity}" /></td>
						<td><c:out value="${elem.pack.packageType}" /></td>
						<td><c:out value="${elem.pack.quantity}" /></td>
						<td><c:out value="${elem.pack.price}" /></td>
						<td><c:out value="${elem.version}" /></td>
						<td><c:out value="${elem.sertificate.sertificateNumber}" /></td>
						<td><c:out value="${elem.sertificate.date}" /></td>
						<td><c:out value="${elem.sertificate.organisation}" /></td>
						<td><c:out value="${elem.vitaminType}" /></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</form>
	<a href="controller?command=logout">Logout</a>
</body>
</html>
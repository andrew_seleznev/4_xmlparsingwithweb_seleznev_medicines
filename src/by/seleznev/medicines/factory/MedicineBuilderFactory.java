package by.seleznev.medicines.factory;

import by.seleznev.medicines.builder.AbstractMedicinesBuilder;
import by.seleznev.medicines.builder.MedicinesDOMBuilder;
import by.seleznev.medicines.builder.MedicinesSAXBuilder;
import by.seleznev.medicines.builder.MedicinesStAXBuilder;
import by.seleznev.medicines.exception.MedicineXMLParseException;

public class MedicineBuilderFactory {

	private enum TypeParser {
		SAX, DOM, STAX
	}
	
	private MedicineBuilderFactory() {
		
	}
	
	private static class SingletonHolder {
		private final static MedicineBuilderFactory INSTANCE = new MedicineBuilderFactory();
	}

	public static MedicineBuilderFactory getInstance() {
		return SingletonHolder.INSTANCE;
	}

	public AbstractMedicinesBuilder createMedicineBuilder(String typeParser) throws MedicineXMLParseException {
		TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
		switch (type) {
		case SAX:
			return new MedicinesSAXBuilder();
		case DOM:
			return new MedicinesDOMBuilder();
		case STAX:
			return new MedicinesStAXBuilder();
		default:
			throw new MedicineXMLParseException("Wrong XML parser type was requested");
		}
	}
}

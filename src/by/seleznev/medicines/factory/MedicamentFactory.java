package by.seleznev.medicines.factory;

import by.seleznev.medicines.entity.Analgesic;
import by.seleznev.medicines.entity.Antibiotic;
import by.seleznev.medicines.entity.Medicament;
import by.seleznev.medicines.entity.Vitamin;
import by.seleznev.medicines.exception.MedicineXMLParseException;

public class MedicamentFactory {

	private enum TypeMedicament {
		ANTIBIOTIC, ANALGESIC, VITAMIN
	}
	
	private MedicamentFactory() {
		
	}
	
	private static class SingletonHolder {
		private final static MedicamentFactory INSTANCE = new MedicamentFactory();
	}

	public static MedicamentFactory getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public Medicament createMedicament(String typeMedicament) throws MedicineXMLParseException {
		TypeMedicament type = TypeMedicament.valueOf(typeMedicament.toUpperCase());
		switch (type) {
		case ANTIBIOTIC:
			return new Antibiotic();
		case ANALGESIC:
			return new Analgesic();
		case VITAMIN:
			return new Vitamin();
		default:
			throw new MedicineXMLParseException("Wrong medicament type was requested");
		}
	}
}

package by.seleznev.medicines.resource;

import java.util.ResourceBundle;

public class ParserManager {
	
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.parser");

	private ParserManager() {
	}

	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}

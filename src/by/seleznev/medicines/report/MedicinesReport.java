package by.seleznev.medicines.report;

import java.util.List;

import by.seleznev.medicines.entity.Medicament;

public class MedicinesReport {
	
	public static void printAllMedicines(List<Medicament> medicines) {
		medicines.forEach(System.out::println);
	}

}

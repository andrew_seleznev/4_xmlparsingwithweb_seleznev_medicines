package by.seleznev.medicines.handler;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import by.seleznev.medicines.entity.Analgesic;
import by.seleznev.medicines.entity.Antibiotic;
import by.seleznev.medicines.entity.Medicament;
import by.seleznev.medicines.entity.Vitamin;
import by.seleznev.medicines.enumeration.AnalgesicEnum;
import by.seleznev.medicines.enumeration.AntibioticEnum;
import by.seleznev.medicines.enumeration.MedicineEnum;
import by.seleznev.medicines.enumeration.PackageEnum;
import by.seleznev.medicines.enumeration.VersionEnum;
import by.seleznev.medicines.enumeration.VitaminEnum;
import by.seleznev.medicines.exception.MedicineXMLParseException;
import by.seleznev.medicines.factory.MedicamentFactory;

public class MedicineHandler extends DefaultHandler {
	
	static Logger logger = LogManager.getLogger(MedicineHandler.class);
	private List<Medicament> medicines;
	private Medicament currentMedicament;
	private MedicineEnum currentMedicineEnum;
	private EnumSet<MedicineEnum> withText;
	private EnumSet<MedicineEnum> medicamentTypes;
	private static final String DATE_TIME_PATTERN = "yyyy-MM-dd";

	public MedicineHandler() {
		medicines = new ArrayList<>();
		withText = EnumSet.range(MedicineEnum.PHARM, MedicineEnum.VITAMIN_TYPE);
		medicamentTypes = EnumSet.range(MedicineEnum.ANTIBIOTIC, MedicineEnum.VITAMIN);
	}

	public List<Medicament> getMedicines() {
		return medicines;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		try {
			if (medicamentTypes.contains(MedicineEnum.getMedicineType(localName))) {
				currentMedicament = MedicamentFactory.getInstance()
						.createMedicament(MedicineEnum.getMedicineType(localName).getType());
				if (attributes.getIndex(MedicineEnum.TITLE.getType()) != -1) {
					currentMedicament.setTitle(attributes.getValue(MedicineEnum.TITLE.getType()));
				}

				if (attributes.getIndex(MedicineEnum.ID.getType()) != -1) {
					currentMedicament.setMedicamentId(attributes.getValue(MedicineEnum.ID.getType()));
				}
			} else {
				MedicineEnum temp = MedicineEnum.getMedicineType(localName);
				if (withText.contains(temp)) {
					currentMedicineEnum = temp;
				}
			}
		} catch (MedicineXMLParseException e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (medicamentTypes.contains(MedicineEnum.getMedicineType(localName))) {
			medicines.add(currentMedicament);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String s = new String(ch, start, length).trim();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
		if (currentMedicineEnum != null) {
			switch (currentMedicineEnum) {
			case PHARM:
				currentMedicament.setPharm(s);
				break;
			case ANALOG:
				currentMedicament.addAnalog(s);
				break;
			case DOSAGE:
				currentMedicament.getDosageRegimen().setDosage(s);
				break;
			case PERIODICITY:
				currentMedicament.getDosageRegimen().setPeriodicity(s);
				break;
			case PACKAGE_TYPE:
				currentMedicament.getPack().setPackageType(PackageEnum.getPackageType(s));
				break;
			case QUANTITY:
				currentMedicament.getPack().setQuantity(Integer.valueOf(s));
				break;
			case PRICE:
				currentMedicament.getPack().setPrice(Double.valueOf(s));
				break;
			case VERSION:
				currentMedicament.setVersion(VersionEnum.getVersionType(s));
				break;
			case NUMBER:
				currentMedicament.getSertificate().setSertificateNumber(s);
				break;
			case DATE_OF_ISSUE:
				currentMedicament.getSertificate().setDate(LocalDate.parse(s, formatter));
				break;
			case ORGANISATION:
				currentMedicament.getSertificate().setOrganisation(s);
				break;
			case CHEMICAL_STRUCTURE:
				((Antibiotic) currentMedicament).setChemicalStructureType(AntibioticEnum.getAntibioticType(s));
				break;
			case ANALGESIC_CLASS:
				((Analgesic) currentMedicament).setClassificationType(AnalgesicEnum.getAnalgesicType(s));
				break;
			case VITAMIN_TYPE:
				((Vitamin) currentMedicament).setVitaminType(VitaminEnum.getVitaminType(s));
				break;
			default:
				throw new EnumConstantNotPresentException(currentMedicineEnum.getDeclaringClass(),
						currentMedicineEnum.name());
			}
		}
		currentMedicineEnum = null;
	}
}

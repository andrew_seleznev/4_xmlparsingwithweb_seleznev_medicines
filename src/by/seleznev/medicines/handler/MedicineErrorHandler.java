package by.seleznev.medicines.handler;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class MedicineErrorHandler extends DefaultHandler{
		
	@Override
	public void warning(SAXParseException e) throws SAXException {
		throw new SAXException(getLineAddress(e) + " - " + e.getMessage());
	}
	
	@Override
	public void error(SAXParseException e) throws SAXException {
		throw new SAXException(getLineAddress(e) + " - " + e.getMessage());
	}
	
	@Override
	public void fatalError(SAXParseException e) throws SAXException {
		throw new SAXException(getLineAddress(e) + " - " + e.getMessage());
	}
	
	private String getLineAddress(SAXParseException e) {
		return e.getLineNumber() + " : " + e.getColumnNumber();
	}
}

package by.seleznev.medicines.command;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.medicines.resource.ConfigurationManager;

public class EmptyCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigurationManager.getProperty("path.page.login");
		return page;
	}
}

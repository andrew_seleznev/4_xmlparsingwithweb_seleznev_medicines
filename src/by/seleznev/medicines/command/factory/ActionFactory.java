package by.seleznev.medicines.command.factory;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.medicines.command.ActionCommand;
import by.seleznev.medicines.command.EmptyCommand;
import by.seleznev.medicines.command.client.CommandEnum;
import by.seleznev.medicines.resource.MessageManager;

public class ActionFactory {
	
	private static final String P_COMMAND = "command";
	private static final String ATTR_WRONG_ACTION = "wrongAction";
	private static final String MESSAGE_WRONG_ACTION = "message.wrongaction";
	
	public ActionCommand defineCommand(HttpServletRequest request) {
		ActionCommand current = new EmptyCommand();
		String action = request.getParameter(P_COMMAND);
		if (action == null || action.isEmpty()) {
			return current;
		}
		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			request.setAttribute(ATTR_WRONG_ACTION, action + MessageManager.getProperty(MESSAGE_WRONG_ACTION));
		}
		return current;
	}
}

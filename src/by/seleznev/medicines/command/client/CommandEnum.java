package by.seleznev.medicines.command.client;

import by.seleznev.medicines.command.ActionCommand;
import by.seleznev.medicines.command.LoginCommand;
import by.seleznev.medicines.command.LogoutCommand;
import by.seleznev.medicines.command.ParseCommand;

public enum CommandEnum {
	LOGIN {
		{
			this.command = new LoginCommand();
		}
	},
	LOGOUT {
		{
			this.command = new LogoutCommand();
		}
	},
	PARSE {
		{
			this.command = new ParseCommand();
		}
	};
	ActionCommand command;

	public ActionCommand getCurrentCommand() {
		return command;
	}
}

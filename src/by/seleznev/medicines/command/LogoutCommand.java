package by.seleznev.medicines.command;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.medicines.resource.ConfigurationManager;

public class LogoutCommand implements ActionCommand {
	private static final String PATH_INDEX_PAGE = "path.page.index";
	
	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigurationManager.getProperty(PATH_INDEX_PAGE);
		request.getSession().invalidate();
		return page;
	}
}

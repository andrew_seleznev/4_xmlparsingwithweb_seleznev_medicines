package by.seleznev.medicines.command;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.medicines.exception.MedicineXMLParseException;

public interface ActionCommand {
	String execute(HttpServletRequest request) throws MedicineXMLParseException;
}

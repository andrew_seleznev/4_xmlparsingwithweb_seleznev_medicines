package by.seleznev.medicines.command;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.seleznev.medicines.builder.AbstractMedicinesBuilder;
import by.seleznev.medicines.entity.Analgesic;
import by.seleznev.medicines.entity.Antibiotic;
import by.seleznev.medicines.entity.Medicament;
import by.seleznev.medicines.entity.Vitamin;
import by.seleznev.medicines.exception.MedicineXMLParseException;
import by.seleznev.medicines.factory.MedicineBuilderFactory;
import by.seleznev.medicines.resource.ConfigurationManager;
import by.seleznev.medicines.resource.ParserManager;
import by.seleznev.medicines.validator.MedicinesValidator;

public class ParseCommand implements ActionCommand {

	static Logger logger = LogManager.getLogger(ParseCommand.class);
	private static final String PARSER_TYPE = "parser";
	private static final String XML_PATH = "xml.path";
	private static final String XSD_PATH = "xsd.path";
	private static final String PATH_MAIN_PAGE = "path.page.main";
	private static final String ANALGESIC = "analgesic";
	private static final String ANTIBIOTIC = "antibiotic";
	private static final String VITAMIN = "vitamin";
	private static final String CURRENT_PARSER_TYPE = "currentParserType";

	@Override
	public String execute(HttpServletRequest request) throws MedicineXMLParseException {
		List<Medicament> analgesic = new ArrayList<>();
		List<Medicament> antibiotic = new ArrayList<>();
		List<Medicament> vitamin = new ArrayList<>();
		String page = null;
		AbstractMedicinesBuilder builder = null;
		ServletContext context = request.getServletContext();
		String xmlPath = context.getRealPath(ParserManager.getProperty(XML_PATH));
		String xsdPath = context.getRealPath(ParserManager.getProperty(XSD_PATH));
		MedicinesValidator.validateMedicines(xmlPath, xsdPath);
		String parserType = request.getParameter(PARSER_TYPE);
		if (!parserType.isEmpty()) {
			request.setAttribute(CURRENT_PARSER_TYPE, parserType);
			builder = MedicineBuilderFactory.getInstance().createMedicineBuilder(parserType);
			builder.buildSetMedicines(xmlPath);
			// MedicinesReport.printAllMedicines(builder.getMedicines());
			List<Medicament> list = builder.getMedicines();
			Iterator<Medicament> iterator = list.iterator();
			while (iterator.hasNext()) {
				Medicament medicament = iterator.next();
				if (medicament.getClass() == Analgesic.class) {
					analgesic.add(medicament);
				} else if (medicament.getClass() == Antibiotic.class) {
					antibiotic.add(medicament);
				} else if (medicament.getClass() == Vitamin.class) {
					vitamin.add(medicament);
				}
			}
			request.setAttribute(ANALGESIC, analgesic);
			request.setAttribute(ANTIBIOTIC, antibiotic);
			request.setAttribute(VITAMIN, vitamin);
		}
		page = ConfigurationManager.getProperty(PATH_MAIN_PAGE);
		return page;
	}
}

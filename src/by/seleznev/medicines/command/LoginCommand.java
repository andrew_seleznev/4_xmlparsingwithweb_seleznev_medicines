package by.seleznev.medicines.command;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.medicines.logic.LoginLogic;
import by.seleznev.medicines.resource.ConfigurationManager;
import by.seleznev.medicines.resource.MessageManager;

public class LoginCommand implements ActionCommand {
	private static final String PARAM_NAME_LOGIN = "login";
	private static final String PARAM_NAME_PASSWORD = "password";
	private static final String ATTR_USER = "user";
	private static final String ATTR_ERROR_LOGIN = "errorLoginPassMessage";
	private static final String MESSAGE_ERROR_LOGIN = "message.loginerror";
	private static final String PATH_MAIN_PAGE = "path.page.main";
	private static final String PATH_LOGIN_PAGE = "path.page.login";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String login = request.getParameter(PARAM_NAME_LOGIN);
		String pass = request.getParameter(PARAM_NAME_PASSWORD);
		if (LoginLogic.checkLogin(login, pass)) {
			request.setAttribute(ATTR_USER, login);
			page = ConfigurationManager.getProperty(PATH_MAIN_PAGE);
		} else {
			request.setAttribute(ATTR_ERROR_LOGIN, MessageManager.getProperty(MESSAGE_ERROR_LOGIN));
			page = ConfigurationManager.getProperty(PATH_LOGIN_PAGE);
		}
		return page;
	}
}

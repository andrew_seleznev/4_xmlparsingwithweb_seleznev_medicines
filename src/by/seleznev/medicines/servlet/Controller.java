package by.seleznev.medicines.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.seleznev.medicines.command.ActionCommand;
import by.seleznev.medicines.command.factory.ActionFactory;
import by.seleznev.medicines.exception.MedicineXMLParseException;
import by.seleznev.medicines.resource.ConfigurationManager;
import by.seleznev.medicines.resource.MessageManager;

@WebServlet("/controller")
public class Controller extends HttpServlet {
	
	static Logger logger = LogManager.getLogger(Controller.class);
	private static final long serialVersionUID = 1L;
	private static final String ERROR_PAGE_PATH = "path.page.error";
	private static final String INDEX_PAGE_PATH = "path.page.index";
	private static final String MESSAGE_NULL_PAGE = "message.nullpage";
	private static final String ATTR_NULL_PAGE = "nullPage";
	private static final String ATTR_ERROR_MESSAGE = "errorMessage";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String page = null;
		ActionFactory client = new ActionFactory();
		ActionCommand command = client.defineCommand(request);
		try {
			page = command.execute(request);
		} catch (MedicineXMLParseException e) {
			logger.fatal(e.getMessage());
			request.getSession().setAttribute(ATTR_ERROR_MESSAGE, e.getMessage());
			page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
		}
		if (page != null) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			dispatcher.forward(request, response);
		} else {
			page = ConfigurationManager.getProperty(INDEX_PAGE_PATH);
			request.getSession().setAttribute(ATTR_NULL_PAGE, MessageManager.getProperty(MESSAGE_NULL_PAGE));
			response.sendRedirect(request.getContextPath() + page);
		}
	}
}
package by.seleznev.medicines.validator;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import by.seleznev.medicines.exception.MedicineXMLParseException;
import by.seleznev.medicines.handler.MedicineErrorHandler;

public class MedicinesValidator {
	
	static Logger logger = LogManager.getLogger(MedicinesValidator.class);

	public static void validateMedicines(String xmlPath, String xsdPath) throws MedicineXMLParseException {
		String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
		SchemaFactory factory = SchemaFactory.newInstance(language);
		File schemaLocation = new File(xsdPath);

		try {
			Schema schema = factory.newSchema(schemaLocation);
			Validator validator = schema.newValidator();
			Source source = new StreamSource(xmlPath);
			MedicineErrorHandler sh = new MedicineErrorHandler();
			validator.setErrorHandler(sh);
			validator.validate(source);
			logger.info(xmlPath + " validation was completed successfully.");
		} catch (SAXException e) {
			logger.fatal("validation " + xmlPath + " is not valid because " + e.getMessage());
			throw new MedicineXMLParseException("validation " + xmlPath + " is not valid because " + e.getMessage());
		} catch (IOException e) {
			logger.fatal(xmlPath + " is not valid because " + e.getMessage());
			throw new MedicineXMLParseException(xmlPath + " is not valid because " + e.getMessage());
		}
	}

}

package by.seleznev.medicines.enumeration;

public enum PackageEnum {
	
	STRIP("strip-packaging"), BOTTLE("bottle-packaging");
	
	private String typeName;

	private PackageEnum(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public static PackageEnum getPackageType(String typeName) {
		for (PackageEnum element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
}

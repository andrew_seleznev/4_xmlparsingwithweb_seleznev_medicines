package by.seleznev.medicines.enumeration;

public enum VersionEnum {

	PILLS("pills"), CAPSULES("capsules"), POWDER("powder"), DROPS("drops");

	private String typeName;

	private VersionEnum(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public static VersionEnum getVersionType(String typeName) {
		for (VersionEnum element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
	
}

package by.seleznev.medicines.enumeration;

public enum MedicineEnum {
	
	MEDICINES("medicines"),
	ANTIBIOTIC("antibiotic"),
	ANALGESIC("analgesic"),
	VITAMIN("vitamin"),
	TITLE("title"),
	ID("id"),
	
	PHARM("pharm"),
	ANALOG("analog"),
	DOSAGE("dosage"),
	PERIODICITY("periodicity"),
	PACKAGE_TYPE("package-type"),
	QUANTITY("quantity"),
	PRICE("price"),
	VERSION("version"),
	NUMBER("number"),
	DATE_OF_ISSUE("date-of-issue"),
	ORGANISATION("organisation"),
	CHEMICAL_STRUCTURE("chemical-structure"),
	ANALGESIC_CLASS("analgesic-class"),
	VITAMIN_TYPE("vitamin-type"),
	
	DOSAGE_REGIMENT("dosage-regimen"),
	PACK("pack"),
	SERTIFICATE("sertificate");
	
	private String type;

	private MedicineEnum(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	public static MedicineEnum getMedicineType(String typeName) {
		for (MedicineEnum element : values()) {
			if (typeName.equals(element.getType())) {
				return element;
			}
		}
		return null;
	}
	
}

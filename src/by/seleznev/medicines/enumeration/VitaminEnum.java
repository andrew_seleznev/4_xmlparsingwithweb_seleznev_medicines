package by.seleznev.medicines.enumeration;

public enum VitaminEnum {

	A_GROUP("vitamin-A-group"), B_GROUP("vitamin-B-group"), C_GROUP("vitamin-C-group"), D_GROUP(
			"vitamin-D-group"), E_GROUP("vitamin-E-group"), K_GROUP("vitamin-K-group");

	private String typeName;

	private VitaminEnum(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public static VitaminEnum getVitaminType(String typeName) {
		for (VitaminEnum element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
}

package by.seleznev.medicines.enumeration;

public enum AnalgesicEnum {
	
	NSAID("NSAID (Mid analgesic)"), OPIOID("OPIOID (Strong analgesic)");
	
	private String typeName;

	private AnalgesicEnum(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public static AnalgesicEnum getAnalgesicType(String typeName) {
		for (AnalgesicEnum element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
	
}

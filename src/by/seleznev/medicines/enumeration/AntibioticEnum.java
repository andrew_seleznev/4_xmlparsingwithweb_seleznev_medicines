package by.seleznev.medicines.enumeration;

public enum AntibioticEnum {

	B_LACTAMS("B-Lactams"), AMINOGLYCOSIDES("Aminoglycosides"), CHLORAMPHENICOL("Chloramphenicol"), GLYCOPEPTIDES(
			"Glycopeptides"), QUINOLONES("Quinolones"), OXAZOLIDINONES("Oxazolidinones");

	private String typeName;

	private AntibioticEnum(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public static AntibioticEnum getAntibioticType(String typeName) {
		for (AntibioticEnum element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
	
}

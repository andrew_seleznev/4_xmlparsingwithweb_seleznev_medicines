package by.seleznev.medicines.builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.seleznev.medicines.entity.Analgesic;
import by.seleznev.medicines.entity.Antibiotic;
import by.seleznev.medicines.entity.Medicament;
import by.seleznev.medicines.entity.Vitamin;
import by.seleznev.medicines.enumeration.AnalgesicEnum;
import by.seleznev.medicines.enumeration.AntibioticEnum;
import by.seleznev.medicines.enumeration.MedicineEnum;
import by.seleznev.medicines.enumeration.PackageEnum;
import by.seleznev.medicines.enumeration.VersionEnum;
import by.seleznev.medicines.enumeration.VitaminEnum;
import by.seleznev.medicines.exception.MedicineXMLParseException;
import by.seleznev.medicines.factory.MedicamentFactory;

public class MedicinesStAXBuilder extends AbstractMedicinesBuilder {
	
	static Logger logger = LogManager.getLogger(MedicinesStAXBuilder.class);
	private XMLInputFactory inputFactory;
	private EnumSet<MedicineEnum> medicamentTypes;
	private static final String DATE_TIME_PATTERN = "yyyy-MM-dd";

	public MedicinesStAXBuilder() {
		inputFactory = XMLInputFactory.newInstance();
		medicamentTypes = EnumSet.range(MedicineEnum.ANTIBIOTIC, MedicineEnum.VITAMIN);
	}

	@Override
	public void buildSetMedicines(String fileName) {
		FileInputStream inputStream = null;
		XMLStreamReader reader = null;
		String name;

		try {
			inputStream = new FileInputStream(new File(fileName));
			reader = inputFactory.createXMLStreamReader(inputStream);
			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();
					if (medicamentTypes.contains(MedicineEnum.getMedicineType(name))) {
						Medicament medicament = MedicamentFactory.getInstance()
								.createMedicament(MedicineEnum.getMedicineType(name).getType());
						medicament = buildMedicament(reader, medicament);
						medicines.add(medicament);
					}
				}
			}
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
		} catch (XMLStreamException e) {
			logger.error(e.getMessage());
		} catch (MedicineXMLParseException e) {
			logger.error(e.getMessage());
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}

	private Medicament buildMedicament(XMLStreamReader reader, Medicament medicament) throws MedicineXMLParseException {
		String name;
		medicament = setMedicamentAttributes(reader, medicament);
		try {
			while (reader.hasNext()) {
				int type = reader.next();
				switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();
					switch (MedicineEnum.getMedicineType(name)) {
					case PHARM:
						medicament.setPharm(getXMLText(reader));
						break;
					case ANALOG:
						medicament.addAnalog(getXMLText(reader));
						break;
					case DOSAGE_REGIMENT:
						medicament = getXMLDosageRegiment(reader, medicament);
						break;
					case PACK:
						medicament = getXMLPack(reader, medicament);
						break;
					case VERSION:
						medicament.setVersion(VersionEnum.getVersionType(getXMLText(reader)));
						break;
					case SERTIFICATE:
						medicament = getXMLSertificate(reader, medicament);
						break;
					case CHEMICAL_STRUCTURE:
						((Antibiotic) medicament)
								.setChemicalStructureType(AntibioticEnum.getAntibioticType(getXMLText(reader)));
						break;
					case ANALGESIC_CLASS:
						((Analgesic) medicament)
								.setClassificationType(AnalgesicEnum.getAnalgesicType(getXMLText(reader)));
						break;
					case VITAMIN_TYPE:
						((Vitamin) medicament).setVitaminType(VitaminEnum.getVitaminType(getXMLText(reader)));
						break;
					default:
						throw new MedicineXMLParseException("Required Enum constant not present");
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (medicamentTypes.contains(MedicineEnum.getMedicineType(name))) {
						return medicament;
					}
					break;
				}
			}
		} catch (XMLStreamException e) {
			throw new MedicineXMLParseException(e);
		}
		return medicament;
	}

	private Medicament getXMLDosageRegiment(XMLStreamReader reader, Medicament medicament)
			throws MedicineXMLParseException {
		int type;
		String name;
		try {
			while (reader.hasNext()) {
				type = reader.next();
				switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();
					switch (MedicineEnum.getMedicineType(name)) {
					case DOSAGE:
						medicament.getDosageRegimen().setDosage(getXMLText(reader));
						break;
					case PERIODICITY:
						medicament.getDosageRegimen().setPeriodicity(getXMLText(reader));
						break;
					default:
						throw new MedicineXMLParseException("Required Enum constant not present");
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (MedicineEnum.getMedicineType(name).equals(MedicineEnum.DOSAGE_REGIMENT)) {
						return medicament;
					}
					break;
				}
			}
		} catch (XMLStreamException e) {
			throw new MedicineXMLParseException(e);
		}
		return medicament;
	}

	private Medicament getXMLPack(XMLStreamReader reader, Medicament medicament) throws MedicineXMLParseException {
		int type;
		String name;
		try {
			while (reader.hasNext()) {
				type = reader.next();
				switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();
					switch (MedicineEnum.getMedicineType(name)) {
					case PACKAGE_TYPE:
						medicament.getPack().setPackageType(PackageEnum.getPackageType(getXMLText(reader)));
						break;
					case QUANTITY:
						medicament.getPack().setQuantity(Integer.valueOf(getXMLText(reader)));
						break;
					case PRICE:
						medicament.getPack().setPrice(Double.valueOf(getXMLText(reader)));
						break;
					default:
						throw new MedicineXMLParseException("Required Enum constant not present");
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (MedicineEnum.getMedicineType(name).equals(MedicineEnum.PACK)) {
						return medicament;
					}
					break;
				}
			}
		} catch (NumberFormatException | XMLStreamException e) {
			throw new MedicineXMLParseException(e);
		}
		return medicament;

	}

	private Medicament getXMLSertificate(XMLStreamReader reader, Medicament medicament)
			throws MedicineXMLParseException {
		int type;
		String name;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
		try {
			while (reader.hasNext()) {
				type = reader.next();
				switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();
					switch (MedicineEnum.getMedicineType(name)) {
					case NUMBER:
						medicament.getSertificate().setSertificateNumber(getXMLText(reader));
						break;
					case DATE_OF_ISSUE:
						medicament.getSertificate().setDate(LocalDate.parse(getXMLText(reader), formatter));
						break;
					case ORGANISATION:
						medicament.getSertificate().setOrganisation(getXMLText(reader));
						break;
					default:
						throw new MedicineXMLParseException("Required Enum constant not present");
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (MedicineEnum.getMedicineType(name).equals(MedicineEnum.SERTIFICATE)) {
						return medicament;
					}
					break;
				}
			}
		} catch (XMLStreamException e) {
			throw new MedicineXMLParseException(e);
		}
		return medicament;
	}

	private Medicament setMedicamentAttributes(XMLStreamReader reader, Medicament medicament) {
		for (int i = 0, n = reader.getAttributeCount(); i < n; ++i) {
			String name = reader.getAttributeName(i).toString();
			String value = reader.getAttributeValue(i);
			if (name.equals(MedicineEnum.TITLE.getType())) {
				medicament.setTitle(value);
			} else if (name.equals(MedicineEnum.ID.getType())) {
				medicament.setMedicamentId(value);
			}
		}
		return medicament;
	}

	private String getXMLText(XMLStreamReader reader) throws MedicineXMLParseException {
		String text = null;
		try {
			if (reader.hasNext()) {
				reader.next();
				text = reader.getText();
			}
		} catch (XMLStreamException e) {
			throw new MedicineXMLParseException(e);
		}
		return text;
	}

}

package by.seleznev.medicines.builder;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.seleznev.medicines.entity.Analgesic;
import by.seleznev.medicines.entity.Antibiotic;
import by.seleznev.medicines.entity.Medicament;
import by.seleznev.medicines.entity.Vitamin;
import by.seleznev.medicines.enumeration.AnalgesicEnum;
import by.seleznev.medicines.enumeration.AntibioticEnum;
import by.seleznev.medicines.enumeration.MedicineEnum;
import by.seleznev.medicines.enumeration.PackageEnum;
import by.seleznev.medicines.enumeration.VersionEnum;
import by.seleznev.medicines.enumeration.VitaminEnum;
import by.seleznev.medicines.exception.MedicineXMLParseException;
import by.seleznev.medicines.factory.MedicamentFactory;

public class MedicinesDOMBuilder extends AbstractMedicinesBuilder {
	
	static Logger logger = LogManager.getLogger(MedicinesDOMBuilder.class);
	private DocumentBuilder docBuilder;
	private EnumSet<MedicineEnum> medicamentTypes;
	private static final String DATE_TIME_PATTERN = "yyyy-MM-dd";

	public MedicinesDOMBuilder() {
		medicamentTypes = EnumSet.range(MedicineEnum.ANTIBIOTIC, MedicineEnum.VITAMIN);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public void buildSetMedicines(String fileName) {
		Document document = null;
		try {
			document = docBuilder.parse(fileName);
			Element root = document.getDocumentElement();
			medicamentTypes.forEach(p -> {
				NodeList medicinesList = root.getElementsByTagName(p.getType());
				if (medicinesList.getLength() != 0) {
					parseElements(medicinesList, p.name());
				}
			});
		} catch (SAXException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	public void parseElements(NodeList nodeList, String typeName) {

		try {
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element element = (Element) nodeList.item(i);
				Medicament medicine;
				medicine = buildMedicament(element, typeName);
				medicines.add(medicine);
			}
		} catch (MedicineXMLParseException e) {
			logger.error(e.getMessage());
		}

	}

	private Medicament buildMedicament(Element element, String typeName) throws MedicineXMLParseException {
		Medicament medicament = MedicamentFactory.getInstance().createMedicament(typeName.toUpperCase());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
		if (element.hasAttribute(MedicineEnum.TITLE.getType())) {
			medicament.setTitle(element.getAttribute(MedicineEnum.TITLE.getType()));
		}
		if (element.hasAttribute(MedicineEnum.ID.getType())) {
			medicament.setMedicamentId(element.getAttribute(MedicineEnum.ID.getType()));
		}
		medicament.setPharm(getElementTextContent(element, MedicineEnum.PHARM.getType()));
		medicament = getAllElementTextContent(element, MedicineEnum.ANALOG.getType(), medicament);
		medicament.getDosageRegimen().setDosage(getElementTextContent(element, MedicineEnum.DOSAGE.getType()));
		medicament.getDosageRegimen().setPeriodicity(getElementTextContent(element, MedicineEnum.PERIODICITY.getType()));
		medicament.getPack().setPackageType(
				PackageEnum.getPackageType(getElementTextContent(element, MedicineEnum.PACKAGE_TYPE.getType())));
		medicament.getPack().setQuantity(Integer.valueOf(getElementTextContent(element, MedicineEnum.QUANTITY.getType())));
		medicament.getPack().setPrice(Double.valueOf(getElementTextContent(element, MedicineEnum.PRICE.getType())));
		medicament
				.setVersion(VersionEnum.getVersionType(getElementTextContent(element, MedicineEnum.VERSION.getType())));
		medicament.getSertificate().setSertificateNumber(getElementTextContent(element, MedicineEnum.NUMBER.getType()));
		medicament.getSertificate().setDate(
				LocalDate.parse(getElementTextContent(element, MedicineEnum.DATE_OF_ISSUE.getType()), formatter));
		medicament.getSertificate().setOrganisation(getElementTextContent(element, MedicineEnum.ORGANISATION.getType()));
		if (medicament.getClass() == Antibiotic.class) {
			Antibiotic antibiotic = (Antibiotic) medicament;
			antibiotic.setChemicalStructureType(AntibioticEnum
					.getAntibioticType(getElementTextContent(element, MedicineEnum.CHEMICAL_STRUCTURE.getType())));
		} else if (medicament.getClass() == Analgesic.class) {
			Analgesic analgesic = (Analgesic) medicament;
			analgesic.setClassificationType(AnalgesicEnum
					.getAnalgesicType(getElementTextContent(element, MedicineEnum.ANALGESIC_CLASS.getType())));
		} else if (medicament.getClass() == Vitamin.class) {
			Vitamin vitamin = (Vitamin) medicament;
			vitamin.setVitaminType(
					VitaminEnum.getVitaminType(getElementTextContent(element, MedicineEnum.VITAMIN_TYPE.getType())));
		}
		return medicament;
	}

	private static String getElementTextContent(Element element, String name) {
		NodeList nodeList = element.getElementsByTagName(name);
		Node node = nodeList.item(0);
		return node.getTextContent();
	}

	private static Medicament getAllElementTextContent(Element element, String name, Medicament medicament) {
		NodeList nodeList = element.getElementsByTagName(name);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			medicament.addAnalog(node.getTextContent());
		}
		return medicament;
	}
}

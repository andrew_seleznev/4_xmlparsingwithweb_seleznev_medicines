package by.seleznev.medicines.builder;

import java.util.ArrayList;
import java.util.List;

import by.seleznev.medicines.entity.Medicament;

public abstract class AbstractMedicinesBuilder {
	 
	protected List<Medicament> medicines;
	
	public AbstractMedicinesBuilder() {
		medicines = new ArrayList<>();
	}
	
	public List<Medicament> getMedicines() {
		return medicines;
	}
	
	abstract public void buildSetMedicines(String fileName);
}

package by.seleznev.medicines.builder;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import by.seleznev.medicines.handler.MedicineHandler;

public class MedicinesSAXBuilder extends AbstractMedicinesBuilder {
	
	static Logger logger = LogManager.getLogger(MedicinesSAXBuilder.class);
	private MedicineHandler medicineHandler;
	private XMLReader reader;
	
	public MedicinesSAXBuilder() {
		medicineHandler = new MedicineHandler();
		try {
			reader = XMLReaderFactory.createXMLReader();
			reader.setContentHandler(medicineHandler);
		} catch (SAXException e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public void buildSetMedicines(String fileName) {
		try {
			reader.parse(fileName);
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (SAXException e) {
			logger.error(e.getMessage());
		}
		medicines = medicineHandler.getMedicines();
	}
	
}

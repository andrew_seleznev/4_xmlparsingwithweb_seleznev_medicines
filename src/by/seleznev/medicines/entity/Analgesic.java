package by.seleznev.medicines.entity;

import by.seleznev.medicines.enumeration.AnalgesicEnum;

public class Analgesic extends Medicament {
	
	private AnalgesicEnum classificationType;

	public Analgesic() {
		super();
	}

	public AnalgesicEnum getClassificationType() {
		return classificationType;
	}

	public void setClassificationType(AnalgesicEnum classificationType) {
		this.classificationType = classificationType;
	}

	@Override
	public String toString() {
		return super.toString() + "\nClassification Type: " + classificationType.getTypeName();
	}
}

package by.seleznev.medicines.entity;

import java.util.ArrayList;

public class Medicines {
	
	private ArrayList<Medicament> medicamentList;
	
	public Medicines() {
		medicamentList = new ArrayList<>();
	}
	
	public void addMedicament(Medicament medicament) {
		medicamentList.add(medicament);
	}
	
	public ArrayList<Medicament> getMedicamentList() {
		return medicamentList;
	}
	
	@Override
	public String toString() {
		return "Medicines [list=" + medicamentList + "]";
	}
}

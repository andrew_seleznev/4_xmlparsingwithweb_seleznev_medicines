package by.seleznev.medicines.entity;

import by.seleznev.medicines.enumeration.VitaminEnum;

public class Vitamin extends Medicament {
	
	private VitaminEnum vitaminType;

	public Vitamin() {
		super();
	}

	public VitaminEnum getVitaminType() {
		return vitaminType;
	}

	public void setVitaminType(VitaminEnum vitaminType) {
		this.vitaminType = vitaminType;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\nVitamin Type: " + vitaminType.getTypeName();
	}
}

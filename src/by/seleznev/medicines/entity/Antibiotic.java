package by.seleznev.medicines.entity;

import by.seleznev.medicines.enumeration.AntibioticEnum;

public class Antibiotic extends Medicament {
	
	private AntibioticEnum chemicalStructureType;

	public Antibiotic() {
		super();
	}

	public AntibioticEnum getChemicalStructureType() {
		return chemicalStructureType;
	}

	public void setChemicalStructureType(AntibioticEnum chemicalStructureType) {
		this.chemicalStructureType = chemicalStructureType;
	}

	@Override
	public String toString() {
		return super.toString() + "\nChemical Structure: " + chemicalStructureType.getTypeName();
	}
}

package by.seleznev.medicines.entity;

import java.time.LocalDate;
import java.util.HashSet;

import by.seleznev.medicines.enumeration.PackageEnum;
import by.seleznev.medicines.enumeration.VersionEnum;

public abstract class Medicament {
	
	private String title;
	private String medicamentId;
	private String pharm;
	private HashSet<String> analogs = new HashSet<>();
	private DosageRegimen dosageRegimen;
	private Pack pack;
	private VersionEnum version;
	private Sertificate sertificate;

	public Medicament() {
		dosageRegimen = new DosageRegimen();
		pack = new Pack();
		sertificate = new Sertificate();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMedicamentId() {
		return medicamentId;
	}

	public void setMedicamentId(String medicamentId) {
		this.medicamentId = medicamentId;
	}

	public String getPharm() {
		return pharm;
	}

	public void setPharm(String pharm) {
		this.pharm = pharm;
	}

	public void addAnalog(String analog) {
		analogs.add(analog);
	}

	public HashSet<String> getAnalogs() {
		return analogs;
	}

	public VersionEnum getVersion() {
		return version;
	}

	public void setVersion(VersionEnum version) {
		this.version = version;
	}

	public DosageRegimen getDosageRegimen() {
		return dosageRegimen;
	}

	public Pack getPack() {
		return pack;
	}

	public Sertificate getSertificate() {
		return sertificate;
	}

	@Override
	public String toString() {
		return "\nTitle: " + title + "\nID: " + medicamentId + "\nPharm: " + pharm + "\nAnalogs: " + analogs.toString()
				+ dosageRegimen.toString() + pack.toString() + "\nVersion: " + version.getTypeName()
				+ sertificate.toString();
	}

	public class DosageRegimen {

		private String dosage;
		private String periodicity;

		public void setDosage(String dosage) {
			this.dosage = dosage;
		}

		public void setPeriodicity(String periodicity) {
			this.periodicity = periodicity;
		}

		public String getDosage() {
			return dosage;
		}

		public String getPeriodicity() {
			return periodicity;
		}

		@Override
		public String toString() {
			return "\nDosage Regimen:" + "\n\tDosage: " + dosage + "\n\tPeriodicity: " + periodicity + "\n";
		}
	}

	public class Pack {

		private PackageEnum packageType;
		private int quantity;
		private double price;

		public void setPackageType(PackageEnum packageType) {
			this.packageType = packageType;
		}

		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}

		public void setPrice(double price) {
			this.price = price;
		}
		

		public PackageEnum getPackageType() {
			return packageType;
		}

		public int getQuantity() {
			return quantity;
		}

		public double getPrice() {
			return price;
		}

		@Override
		public String toString() {
			return "\nPack:" + "\n\tPackage type: " + packageType.getTypeName() + "\n\tQuantity: " + quantity
					+ "\n\tPrice: " + price + "\n";
		}
	}

	public class Sertificate {

		private String sertificateNumber;
		private LocalDate date;
		private String organisation;

		public void setSertificateNumber(String sertificateNumber) {
			this.sertificateNumber = sertificateNumber;
		}

		public void setDate(LocalDate date) {
			this.date = date;
		}

		public void setOrganisation(String organisation) {
			this.organisation = organisation;
		}
		
		public String getSertificateNumber() {
			return sertificateNumber;
		}

		public LocalDate getDate() {
			return date;
		}

		public String getOrganisation() {
			return organisation;
		}

		@Override
		public String toString() {
			return "\nSertificate:" + "\n\tSertificate Number " + sertificateNumber + "\n\tDate: " + date.toString()
					+ "\n\tOrganisation " + organisation + "\n";
		}

	}
}

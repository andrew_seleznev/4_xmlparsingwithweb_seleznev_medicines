package by.seleznev.medicines.service;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.seleznev.medicines.builder.AbstractMedicinesBuilder;
import by.seleznev.medicines.exception.MedicineXMLParseException;
import by.seleznev.medicines.factory.MedicineBuilderFactory;
import by.seleznev.medicines.report.MedicinesReport;

public class MedicinesService {
	
	static Logger logger = LogManager.getLogger(MedicinesService.class);

	public void doParseMedicines(MedicineBuilderFactory builderFactory, String typeParser, String path) {
		try {
			AbstractMedicinesBuilder builder = builderFactory.createMedicineBuilder(typeParser);
			builder.buildSetMedicines(path);
			MedicinesReport.printAllMedicines(builder.getMedicines());
		} catch (MedicineXMLParseException e) {
			logger.error(e.getMessage());
		}
	}

}

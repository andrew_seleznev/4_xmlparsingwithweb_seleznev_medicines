package by.seleznev.medicines.exception;

public class MedicineXMLParseException extends Exception {

	private static final long serialVersionUID = 1L;

	public MedicineXMLParseException() {
		super();
	}

	public MedicineXMLParseException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MedicineXMLParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public MedicineXMLParseException(String message) {
		super(message);
	}

	public MedicineXMLParseException(Throwable cause) {
		super(cause);
	}

}
